import Vue from "vue";
import uniqid from "uniqid";

export default {
  namespaced: true,
  state: {
    id: uniqid(),
    name: "",
    tagline: "",
    topics: [],
    download: [""],
    thumbnail: "",
    status: false,
    gallery: [],
    youtube: "",
    description: "",
    isMaker: false,
    maker: "",
    comment: ""
  },
  mutations: {
    state: (state, payload) => {
      let { value, component } = payload;
      let temp = state;
      temp[component] = value;

      state = {
        ...temp
      };
    },
    replace: (state, payload) => {
      let { value } = payload;
      console.log("hi");
      state = value;
      console.log(state);
    },
    addState: (state, payload) => {
      let { value, component } = payload;
      let temp = state;
      temp[component] = temp[component].concat(value);

      state = {
        ...temp
      };
    },
    removeState: (state, payload) => {
      let { value, component } = payload;
      let temp = state;
      temp[component] = temp[component].filter(x => x !== value);

      state = {
        ...temp
      };
    },
    removeByIndexState: (state, payload) => {
      let { index, component } = payload;
      let temp = state;
      temp[component] = temp[component].filter((x, i) => i !== index);

      state = {
        ...temp
      };
    },
    changeWithIndexState: (state, payload) => {
      let { value, index, component } = payload;
      let temp = state[component];
      temp[index] = value;
      Vue.set(state, component, [...temp]);
    }
  },
  actions: {
    state: ({ commit }, payload) => {
      let { to } = payload;
      if (to === "change") {
        commit("state", payload);
      } else if (to === "add") {
        commit("addState", payload);
      } else if (to === "remove") {
        commit("removeState", payload);
      } else if (to === "removeByIndex") {
        commit("removeByIndexState", payload);
      } else if (to === "changeWithIndex") {
        commit("changeWithIndexState", payload);
      } else if (to === "replace") {
        console.log("hi");
        commit("replace", payload);
      }
    }
  },
  getters: {
    state: function(state) {
      return state;
    },
    download: function(state) {
      return state.download;
    }
  }
};
