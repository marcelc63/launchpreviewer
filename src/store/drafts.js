export default {
  namespaced: true,
  state: {
    drafts: []
  },
  mutations: {
    change: (state, payload) => {
      let { value } = payload;
      let index = state.drafts.findIndex(x => x.id === value.id);
      console.log(index);
      if (index === -1) {
        console.log(index);
        state.drafts = state.drafts.concat(value);
        return;
      } else {
        state.drafts[index] = value;
        return;
      }
    },
    add: (state, payload) => {
      let { value } = payload;
      state.drafts = state.drafts.concat(value);
    },
    remove: (state, payload) => {
      let { value } = payload;
      state.drafts = state.drafts.filter(x => x !== value);
    }
  },
  actions: {
    state: ({ commit }, payload) => {
      let { to } = payload;
      if (to === "change") {
        commit("change", payload);
      } else if (to === "add") {
        commit("add", payload);
      } else if (to === "remove") {
        commit("remove", payload);
      }
    },
    load({ commit }, payload) {
      commit("change", payload);
    },
    save({ commit }, payload) {
      let { key, data } = payload;

      commit("change", payload);
      saveStorage(key, data);
    },
    nav({ commit }) {
      commit("nav");
    }
  },
  getters: {
    drafts: function(state) {
      return state.drafts;
    }
  }
};
