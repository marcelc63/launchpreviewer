import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import store from "@/store/store.js";
import topics from "@/store/topics.js";
import drafts from "@/store/drafts.js";

export default new Vuex.Store({
  modules: {
    store: store,
    topics: topics,
    drafts: drafts
  }
});
